package Principal;

import Clases.Calificacion;
import Clases.Curso;
import Clases.Estudiantes;
import Clases.Establecimiento;

import java.util.*;

/**
 * Created by Edward on 5/07/2017.
 */
public class ConsoleApp {


    public static void main(String[] args) throws Exception {

       Establecimiento establecimiento = null;
        Scanner sn = new Scanner(System.in);
        boolean salir = false;
        int opcion;


        while (!salir) {
            System.out.println("1. Ingresar Cantidad de Alumnos");
            System.out.println("2. Ingresar Alumnos");
            System.out.println("3. Listar Alumnos");
            System.out.println("4. Ingrese el numero de cursos:");
            System.out.println("5. Ingresar Curso");
            System.out.println("6. Listar Curso");
            System.out.println("7. Ingresar Nota:");
            System.out.println("8. Listar Nota::");
            System.out.println("9. Salir");
            System.out.println("Sistema de Establecimiento");

            try {
                System.out.println("Seleccione una de las opciones");
                opcion = sn.nextInt();

                switch (opcion) {
                    case 1:

                        System.out.println("Cuantos Alumnos desea ingresar.");
                        establecimiento = new Establecimiento(sn.nextInt());
                        break;

                    case 2:


                        System.out.println("Nombre:");
                        Estudiantes estudiantes = new Estudiantes(sn.next());
                        System.out.println("Codigo");
                        estudiantes.setCodigo(sn.next());
                        establecimiento.adicionarEstudiantes(estudiantes);
                        break;

                    case 3:

                        Estudiantes[] listado = establecimiento.getListado();

                        for (int i = 0; i < establecimiento.getCantReal(); i++){
                            System.out.println("Estudiantes #" +(i+1)+ ": "+ listado[i].getNombre()+ " Cod: " + listado[i].getCodigo()+ "\n");
                            System.out.println("--------------");
                        }

                        break;
                    case 4:
                        System.out.println("Cuantos Cursos desea ingresar.");
                        establecimiento = new Establecimiento(sn.nextInt());
                        break;
                    case 5:
                        System.out.println("Nombre:");
                        Curso curso = new Curso(sn.next());
                        System.out.println("Codigo");
                        curso.setCodigoCurso(sn.next());
                        establecimiento.adicionarCurso(curso);
                        break;

                    case 6:
                        Curso[] listadoCurso = establecimiento.getListadoCurso();
                       for (int i = 0; i < establecimiento.getCantReal(); i++){

                            System.out.println("Nombre de Curso #" +(i+1)+ ": "+ listadoCurso[i].getNombreCurso()+ " Cod: " + listadoCurso[i].getCodigoCurso()+ "\n");
                            System.out.println("--------------");
                        }
                    break;

                    case 7:

                        System.out.println("Codigo Alumno:");
                        Calificacion calificacion = new Calificacion(sn.next());
                        calificacion.setCodigoAlumno(sn.next());
                        System.out.println("Codigo Curso:");
                        calificacion.setCodigoCurso(sn.next());
                        System.out.println("Ingrese nota:");
                        calificacion.setCalificacion(sn.nextDouble());
                        if(calificacion.getCalificacion()>=60){
                            calificacion.setAprobado("Aprobado");
                        }
                        else {
                            calificacion.setAprobado("Reprobado");}
                        establecimiento.adicionarCalificacion(calificacion);

                        break;

                    case 8:

                        Calificacion[] listadoCalificacion = establecimiento.getCalificacion();

                      //  Calificacion calificacion1 = new Calificacion(sn.next());
                        for (int i = 0; i < establecimiento.getCantNota(); i++){

                            System.out.println("Cod_Alumno: " +(i+1)+ ": "+ listadoCalificacion[i].getCodigoAlumno()+ " Cod_Curso: " + listadoCalificacion[i].getCodigoCurso()+ " Nota: " + listadoCalificacion[i].getCalificacion() + " : " + listadoCalificacion[i].getAprobado() +"\n" );


                            System.out.println("--------------");
                        }

                        break;

                    case 9:
                        salir = true;
                        break;
                    default:
                        System.out.println("Solo números entre 1 y 9");
                }
            } catch (InputMismatchException e){

                System.out.println("Debes insertar un número");
                sn.next();
            }
        }

    }





}
