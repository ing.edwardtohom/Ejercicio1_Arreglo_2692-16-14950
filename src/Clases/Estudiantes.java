package Clases;

/**
 * Created by Edward on 5/07/2017.
 */
public class Estudiantes {
    private String codigo;
    private String nombre;

    public Estudiantes(String nombre) {
        this.nombre = nombre;
    }

    public Estudiantes(String codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


}
