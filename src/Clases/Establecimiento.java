package Clases;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Edward on 5/07/2017.
 */
public class Establecimiento {

    private Estudiantes[] listado;
    private int cantReal;
    private int cantNota;

    private Curso[] listadoCurso;
    private Calificacion[] listadoCalificacion;

    public Establecimiento(int cantEstudiantes) {
        listado = new Estudiantes[cantEstudiantes];
        listadoCurso = new Curso[cantEstudiantes];
        cantReal = 0;
        listadoCalificacion = new Calificacion[cantEstudiantes];
        cantNota = 0;
    }


    public Calificacion[] getCalificacion(){
        return listadoCalificacion;
    }

    public void setListadoCalificacion(Calificacion[] listadoCalificacion){
        this.listadoCalificacion = listadoCalificacion;
    }


    public Curso[] getListadoCurso(){
        return listadoCurso;
    }

    public void setListadoCurso(Curso[] listadoCurso){
        this.listadoCurso = listadoCurso;
    }

    public Estudiantes[] getListado(){
        return listado;
    }

    public void setListado(Estudiantes[] listado) {
        this.listado = listado;
    }

    public int getCantReal() {
        return cantReal;
    }

    public void setCantReal(int cantReal) {
        this.cantReal = cantReal;
    }

    public int getCantNota() {
        return cantNota;
    }

    public void setCantNota(int cantNota) {
        this.cantNota = cantNota;
    }

    public void adicionarEstudiantes(Estudiantes e) throws Exception{

        if (cantReal < listado.length)
        {
            listado[cantReal] = e;
            cantReal++;
        }
        else
            throw new Exception("Imposible adicionar Estudiantes");
    }

    public void adicionarCurso(Curso c) throws Exception{
        if (cantReal < listado.length)
        {
        listadoCurso[cantReal] = c;
            cantReal++;
        }
        else
            throw new Exception("Imposible adicionar Cursos");
    }

    public void adicionarCalificacion(Calificacion n){

        if (cantNota < listadoCalificacion.length) {
            listadoCalificacion[cantNota] = n;
            cantNota++;
        }


    }

}
