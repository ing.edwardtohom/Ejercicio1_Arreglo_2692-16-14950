package Clases;

/**
 * Created by Edward on 7/07/2017.
 */
public class Calificacion {

    private String codigoAlumno;
    private String codigoCurso;
    private Double calificacion;
    private String aprobado;

    public Calificacion(String codigoAlumno) {
        this.codigoAlumno = codigoAlumno;
    }

    public Calificacion(String codigoAlumno, String codigoCurso, Double calificacion, String aprobado) {
        this.codigoAlumno = codigoAlumno;
        this.codigoCurso = codigoCurso;
        this.calificacion = calificacion;
        this.aprobado = aprobado;
    }

    public String getCodigoAlumno() {
        return codigoAlumno;
    }

    public void setCodigoAlumno(String codigoAlumno) {
        this.codigoAlumno = codigoAlumno;
    }

    public String getCodigoCurso() {
        return codigoCurso;
    }

    public void setCodigoCurso(String codigoCurso) {
        this.codigoCurso = codigoCurso;
    }

    public Double getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(Double calificacion) {
        this.calificacion = calificacion;
    }

    public String getAprobado() {
        return aprobado;
    }

    public void setAprobado(String aprobado) {
        this.aprobado = aprobado;
    }
}
